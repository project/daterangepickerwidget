var jQuery183 = window.jQuery183 = window.jQuery183 || jQuery;
jQuery.noConflict(true);

(function ($, Drupal, drupalSettings) {
    "use strict";

    Drupal.behaviors.daterangepicker = {
        attach: function (context, settings) {
            $('.daterangepicker', context).each(function (index, element) {
                // wrap this selector with jQuery v1.8.3 so that we can initialize it with the daterangepicker plugin.
                // all daterangepicker events will be raised on this object.
                var $el = jQuery183(this);
                // whether the daterangepicker widget is used as a better exposed filter?
                var isBEF = false;

                /**
                 * The presetRanges option requires a JSON object.
                 * When setting this option in drupalSettings, we need to encode to a JSON string.
                 *
                 * FIXME: find a better way to set presetRanges
                 */
                if ('presetRanges' in drupalSettings.daterangepicker && typeof drupalSettings.daterangepicker.presetRanges === 'string') {
                    var presetRanges = [];
                    $.each(JSON.parse(drupalSettings.daterangepicker.presetRanges), function (index, item) {
                        var ds = (new Function('return ' + item.dateStart)());
                        var de = (new Function('return ' + item.dateEnd)());
                        presetRanges.push({
                            text: item.text,
                            dateStart: ds,
                            dateEnd: de
                        });
                    });
                    //delete drupalSettings.daterangepicker.presetRanges;
                    drupalSettings.daterangepicker.presetRanges = presetRanges;
                }

                $el.daterangepicker(drupalSettings.daterangepicker);

                // Calculate if we need to set a default value.
                if (drupalSettings.daterangepicker.defaultValue instanceof Object) {
                    var range = {};
                    range.start = new Date(drupalSettings.daterangepicker.defaultValue.start);
                    if (drupalSettings.daterangepicker.defaultValue.end.length > 0) {
                        range.end = new Date(drupalSettings.daterangepicker.defaultValue.end);
                    }
                    $el.daterangepicker('setRange', range);
                }

                // Better Exposed Filter setup
                if (drupalSettings?.better_exposed_filters?.daterangepicker?.field_id !== undefined) {
                    var fieldId = drupalSettings.better_exposed_filters.daterangepicker.field_id;
                    var $container = $el.closest('.views-exposed-form', context);
                    var $textfieldMin = $('.bef-daterangepicker[data-daterangepart="start"][name="' + fieldId + '[min]"]', $container);
                    var $textfieldMax = $('.bef-daterangepicker[data-daterangepart="end"][name="' + fieldId + '[max]"]', $container);
                    isBEF = true;
                }

                $el.on('daterangepickerchange', function () {
                    if (isBEF) {
                        var val = $.parseJSON($(this).val());
                        $textfieldMin.val(val.start + '00:00:00');
                        $textfieldMax.val(val.end + '23:59:59');
                    }
                });

                $el.on('daterangepickerclear', function () {
                    if (isBEF) {
                        $(this).val('');
                        $textfieldMin.val('');
                        $textfieldMax.val('');
                    }
                });

                // freeze scrolling on modal dialogs having daterangepicker widgets when dropdown is opened.
                //
                $el.on('daterangepickeropen', function () {
                    var $exposedFormModal = $(".js-views-ui-dialog");
                    if ($exposedFormModal.length) {
                        $exposedFormModal.addClass('datarangepicker-dropdown-open');
                        $('body').css('overflow-y', 'hidden');
                    }

                });

                $el.on('daterangepickerclose', function () {
                    var $exposedFormModal = $(".js-views-ui-dialog");
                    if ($exposedFormModal.length && $exposedFormModal.hasClass('datarangepicker-dropdown-open')) {
                        $exposedFormModal.removeClass('datarangepicker-dropdown-open');
                        $('body').css('overflow-y', '');
                    }
                });
            });
        }
    };
})(jQuery, Drupal, drupalSettings);
