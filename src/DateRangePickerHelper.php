<?php

namespace Drupal\daterangepickerwidget;

/**
 * Helper class for JQuery UI Date Range Picker.
 */
class DateRangePickerHelper {

  /**
   * Return a list of default options used in the Drupal API.
   *
   * @return array
   *   An array containing the default options.
   */
  public static function getDefaultOptions() {
    // API Options: https://tamble.github.io/jquery-ui-daterangepicker/
    return [
      'initial_text' => t('Select date range...'),
      'apply_button_text' => t('Apply'),
      'clear_button_text' => t('Clear'),
      'cancel_button_text' => t('Cancel'),
      'range_splitter' => ' - ',
      'date_format' => 'd M, yy',
      'number_of_months' => 2,
    ];
  }

  /**
   * Map Drupal API options to equivalent options used in the Javascript API.
   *
   * @return array
   *   An array containing the equivalent Javascript options.
   */
  protected static function mapDrupalToJavascriptOptions() {
    return [
      'initial_text' => 'initialText',
      'apply_button_text' => 'applyButtonText',
      'clear_button_text' => 'clearButtonText',
      'cancel_button_text' => 'cancelButtonText',
      'range_splitter' => 'rangeSplitter',
      'date_format' => 'dateFormat',
      // This option is deliberately omitted from ::getDefaultOptions().
      'alt_format' => 'altFormat',
      'number_of_months' => ['datepickerOptions' => 'numberOfMonths'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function buildOptionsForm(array &$form, array $default_values) {
    $form['initial_text'] = [
      '#type' => 'textfield',
      '#title' => t('Initial text'),
      '#default_value' => $default_values['initial_text'],
    ];
    $form['apply_button_text'] = [
      '#type' => 'textfield',
      '#title' => t('Apply button text'),
      '#default_value' => $default_values['apply_button_text'],
    ];
    $form['clear_button_text'] = [
      '#type' => 'textfield',
      '#title' => t('Clear button text'),
      '#default_value' => $default_values['clear_button_text'],
    ];
    $form['cancel_button_text'] = [
      '#type' => 'textfield',
      '#title' => t('Cancel button text'),
      '#default_value' => $default_values['cancel_button_text'],
    ];
    $form['range_splitter'] = [
      '#type' => 'textfield',
      '#title' => t('Range splitter'),
      '#default_value' => $default_values['range_splitter'],
    ];
    $form['date_format'] = [
      '#type' => 'textfield',
      '#title' => t('Date format'),
      '#description' => t('See <a href="https://api.jqueryui.com/datepicker/#utility-formatDate" target="_blank">the documentation for JQuery UI datepicker formats</a>.'),
      '#default_value' => $default_values['date_format'],
    ];
    $form['number_of_months'] = [
      '#type' => 'number',
      '#title' => t('Number of months'),
      '#default_value' => $default_values['number_of_months'],
    ];
  }

  /**
   * Get summary of the data array.
   *
   * @param array $data
   *   The data array.
   *
   * @return array
   *   The summary of the data.
   */
  public static function getSummary(array $data) {
    $summary = [];
    foreach ($data as $option => $value) {
      $summary[] = t('@option: @value', [
        '@option' => ucwords(str_replace('_', ' ', $option)),
        '@value' => $value,
      ]);
    }
    return $summary;
  }

  /**
   * Map Drupal API options to Javascript API options and update drupalSettings.
   *
   * @param array $drupalSettings
   *   The Drupal settings array.
   * @param array $data
   *   The data array.
   */
  public static function setJavascriptApiOptions(array &$drupalSettings, array $data) {
    $optionsMap = static::mapDrupalToJavascriptOptions();
    foreach ($data as $option => $value) {
      static::setJavascriptApiOption($drupalSettings, $optionsMap[$option], $value);
    }
  }

  /**
   * Set the JavaScript API option in the drupalSettings array.
   *
   * @param array $drupalSettings
   *   The Drupal settings array.
   * @param string|array $key
   *   The key for the settings.
   * @param mixed $value
   *   The value to be set.
   */
  protected static function setJavascriptApiOption(array &$drupalSettings, $key, $value) {
    if (is_array($key)) {
      $drupalSettings[key($key)] = [];
      static::setJavascriptApiOption($drupalSettings[key($key)], current($key), $value);
    }
    else {
      $drupalSettings[$key] = $value;
    }
  }

}
