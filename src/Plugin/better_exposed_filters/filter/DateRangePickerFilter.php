<?php

namespace Drupal\daterangepickerwidget\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\FilterWidgetBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\daterangepickerwidget\DateRangePickerHelper;

/**
 * DateRangePicker views filter implementation.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "bef_daterangepicker",
 *   label = @Translation("jQuery UI Date Range Picker"),
 * )
 */
class DateRangePickerFilter extends FilterWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable($filter = NULL, array $filter_options = []) {
    $is_applicable = FALSE;
    $valid_operators = ['between', 'not between'];
    if ((is_a($filter, 'Drupal\views\Plugin\views\filter\Date') || !empty($filter->date_handler)) && !$filter->isAGroup() && in_array($filter->operator, $valid_operators)) {
      $is_applicable = TRUE;
    }

    return $is_applicable;
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state) {
    $field_id = $this->getExposedFilterFieldId();

    // Handle wrapper element added to exposed filters
    // in https://www.drupal.org/project/drupal/issues/2625136.
    $wrapper_id = $field_id . '_wrapper';
    $element = NULL;
    if (!isset($form[$field_id]) && isset($form[$wrapper_id])) {
      $element = &$form[$wrapper_id][$field_id];
    }
    else {
      $element = &$form[$field_id];
    }

    parent::exposedFormAlter($form, $form_state);

    $daterangepicker_element = [
      '#type' => 'textfield',
      '#title' => $form[$wrapper_id]['#title'] ?? '',
      '#attributes' => [
        'class' => ['daterangepicker'],
        'autocomplete' => 'off',
      ],
      '#attached' => [
        'library' => [
          'daterangepickerwidget/jquery-ui-daterangepicker',
        ],
        'drupalSettings' => [
          'daterangepicker' => [],
        ],
      ],
    ];

    $config = $this->getDateRangePickerConfiguration();
    // This is the internal storage format and should not be altered by a user.
    // Using CCYY-MM-DD format which will be converted by the JS to
    // CCYY-MM-DD HH:MM:SS format required by views.
    $config['alt_format'] = 'yy-mm-dd';

    DateRangePickerHelper::setJavascriptApiOptions($daterangepicker_element['#attached']['drupalSettings']['daterangepicker'], $config);

    // BEF settings.
    $daterangepicker_element['#attached']['drupalSettings']['better_exposed_filters']['daterangepicker']['field_id'] = Html::cleanCssIdentifier($field_id);

    $form[$wrapper_id]["{$field_id}_daterangepicker"] = $daterangepicker_element;

    $element['min']['#type'] = 'hidden';
    $element['min']['#attributes']['class'][] = 'bef-daterangepicker';
    $element['min']['#attributes']['data-daterangepart'][] = 'start';

    $element['max']['#type'] = 'hidden';
    $element['max']['#attributes']['class'][] = 'bef-daterangepicker';
    $element['max']['#attributes']['data-daterangepart'] = 'end';

    unset($element['min']['#title'], $element['max']['#title']);
    $form[$wrapper_id][$wrapper_id]['#attributes']['class'][] = 'visually-hidden';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'daterangepicker' => DateRangePickerHelper::getDefaultOptions(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['daterangepicker'] = [
      '#type' => 'details',
      '#title' => $this->t('Date Range Picker options'),
    ];

    $default_values = $this->getDateRangePickerConfiguration();
    DateRangePickerHelper::buildOptionsForm($form['daterangepicker'], $default_values);
    return $form;
  }

  /**
   * Get the configuration based on the widget's configuration.
   *
   * @return array
   *   The configuration array for the Date Range Picker widget.
   */
  protected function getDateRangePickerConfiguration() {
    $config = [];
    $options = array_keys(DateRangePickerHelper::getDefaultOptions());
    foreach ($options as $option) {
      $config[$option] = $this->configuration['daterangepicker'][$option];
      if (is_numeric($config[$option])) {
        settype($config[$option], 'integer');
      }
    }
    return $config;
  }

}
