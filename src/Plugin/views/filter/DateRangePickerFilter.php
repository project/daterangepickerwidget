<?php

namespace Drupal\daterangepickerwidget\Plugin\views\filter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\daterangepickerwidget\DateRangePickerHelper;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter to handle filtering for the daterangepicker field type.
 *
 * @ViewsFilter("views_daterangepicker")
 */
class DateRangePickerFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['operator'] = ['default' => 'subset'];
    $options['daterangepicker'] = ['default' => DateRangePickerHelper::getDefaultOptions()];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);

    // Build subform to configure daterangepicker config when widget is exposed.
    $form['daterangepicker'] = [
      '#type' => 'details',
      '#title' => $this->t('Date Range Picker options'),
    ];
    $daterangepicker_config = $this->getDateRangePickerConfiguration();
    DateRangePickerHelper::buildOptionsForm($form['daterangepicker'], $daterangepicker_config);
  }

  /**
   * Return configurations for the widget.
   */
  protected function getDateRangePickerConfiguration() {
    $config = [];
    $options = array_keys(DateRangePickerHelper::getDefaultOptions());
    foreach ($options as $option) {
      // Use the same option value irrespective of whether widget is exposed.
      $config[$option] = $this->options['daterangepicker'][$option];
      if (is_numeric($config[$option])) {
        settype($config[$option], 'integer');
      }
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    return [
      'subset' => [
        'title' => $this->t('Is subset of'),
        'short' => $this->t('subset'),
        'method' => 'opSubset',
        'values' => 1,
      ],
      'superset' => [
        'title' => $this->t('Is superset of'),
        'short' => $this->t('superset'),
        'method' => 'opSuperset',
        'values' => 1,
      ],
      'intersect' => [
        'title' => $this->t('Intersects'),
        'short' => $this->t('intersect'),
        'method' => 'opIntersect',
        'values' => 1,
      ],
      'not_intersect' => [
        'title' => $this->t('Does not intersect'),
        'short' => $this->t('not_intersect'),
        'method' => 'opNotIntersect',
      ],
    ];
  }

  /**
   * Build strings from the operators() for 'select' options.
   */
  public function operatorOptions($which = 'title') {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if ($this->isAGroup()) {
      return $this->t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return $this->t('exposed');
    }

    $options = $this->operatorOptions('short');
    $output = '';
    if (!empty($options[$this->operator])) {
      $output = $options[$this->operator];
    }
    if (in_array($this->operator, $this->operatorValues(1))) {
      $output .= ' ' . $this->value;
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  protected function operatorValues($values = 1) {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      if (isset($info['values']) && $info['values'] == $values) {
        $options[] = $id;
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#size' => 30,
      '#default_value' => $this->value,
      '#attributes' => [
        'class' => ['daterangepicker'],
        'autocomplete' => 'off',
      ],
      '#attached' => [
        'library' => [
          'daterangepickerwidget/jquery-ui-daterangepicker',
        ],
        'drupalSettings' => [
          'daterangepicker' => [],
        ],
      ],
    ];

    $daterangepicker_config = $this->getDateRangePickerConfiguration();
    $daterangepicker_config['alt_format'] = 'yy-mm-dd';
    DateRangePickerHelper::setJavascriptApiOptions($form['value']['#attached']['drupalSettings']['daterangepicker'], $daterangepicker_config);
  }

  /**
   * {@inheritdoc}
   */
  protected function valueValidate($form, FormStateInterface $form_state) {
    parent::valueValidate($form, $form_state);

    $options = $form_state->getValue('options');

    $value = $options['value'];
    if (!$this->options['exposed'] && empty($value)) {
      $form_state->setError($form['value'], $this->t('Value is required.'));
    }

    /*
    if ($this->options['exposed']) {
    $daterangepicker_values = $options['daterangepicker'];
    // @todo validate datarangepicker values.
    }
     */
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }

  /**
   * Logic for the subset operator.
   *
   * @param string $field
   *   The fully qualified field name.
   *   $field is a subset of the range value selected in the Value field.
   */
  public function opSubset($field) {
    if (!empty($this->value[0])) {
      ['start' => $start, 'end' => $end] = Json::decode($this->value[0]);

      $this->query->addWhereExpression($this->options['group'], "JSON_EXTRACT({$field}, '$.start') >= :start", [':start' => $start]);
      $this->query->addWhereExpression($this->options['group'], "JSON_EXTRACT({$field}, '$.end') <= :end", [':end' => $end]);
    }
  }

  /**
   * Logic for the superset operator.
   *
   * @param string $field
   *   The fully qualified field name.
   *   $field is a superset of the range value selected in the Value field.
   */
  public function opSuperset($field) {
    if (!empty($this->value[0])) {
      ['start' => $start, 'end' => $end] = Json::decode($this->value[0]);

      $this->query->addWhereExpression($this->options['group'], "JSON_EXTRACT({$field}, '$.start') <= :start", [':start' => $start]);
      $this->query->addWhereExpression($this->options['group'], "JSON_EXTRACT({$field}, '$.end') >= :end", [':end' => $end]);
    }
  }

  /**
   * Logic for the intersect operator.
   *
   * @param string $field
   *   The fully qualified field name.
   */
  public function opIntersect($field) {
    if (!empty($this->value[0])) {
      ['start' => $start, 'end' => $end] = Json::decode($this->value[0]);
      $connection = $this->query->getConnection();

      // Field intersects Value on right.
      $and_cond1 = ($connection->condition('AND'))
        ->where("JSON_EXTRACT({$field}, '$.start') >= :start", [':start' => $start])
        ->where("JSON_EXTRACT({$field}, '$.start') <= :end", [':end' => $end]);

      // Field intersects Value on left.
      $and_cond2 = ($connection->condition('AND'))
        ->where("JSON_EXTRACT({$field}, '$.end') >= :start", [':start' => $start])
        ->where("JSON_EXTRACT({$field}, '$.end') <= :end", [':end' => $end]);

      $or_cond1 = ($connection->condition('OR'))
        ->condition($and_cond1)
        ->condition($and_cond2);

      // Value intersects field on the left.
      $and_cond3 = ($connection->condition('AND'))
        ->where(":end >= JSON_EXTRACT({$field}, '$.start')", [':end' => $end])
        ->where(":end <= JSON_EXTRACT({$field}, '$.end')", [':end' => $end]);

      // Value intersects field on the right.
      $and_cond4 = ($connection->condition('AND'))
        ->where(":start >= JSON_EXTRACT({$field}, '$.start')", [':start' => $start])
        ->where(":start <= JSON_EXTRACT({$field}, '$.end')", [':start' => $start]);

      $or_cond2 = ($connection->condition('OR'))
        ->condition($and_cond3)
        ->condition($and_cond4);

      $or_cond3 = ($connection->condition('OR'))
        ->condition($or_cond1)
        ->condition($or_cond2);

      $this->query->addWhere($this->options['group'], $or_cond3);
    }
  }

  /**
   * Logic for the not_intersect operator.
   *
   * @param string $field
   *   The fully qualified field name.
   */
  public function opNotIntersect($field) {
    if (!empty($this->value[0])) {
      ['start' => $start, 'end' => $end] = Json::decode($this->value[0]);
      $connection = $this->query->getConnection();

      $or_cond = ($connection->condition('OR'))
        ->where("JSON_EXTRACT({$field}, '$.end') < :start", [':start' => $start])
        ->where("JSON_EXTRACT({$field}, '$.start') > :end", [':end' => $end]);

      $this->query->addWhere($this->options['group'], $or_cond);
    }
  }

}
