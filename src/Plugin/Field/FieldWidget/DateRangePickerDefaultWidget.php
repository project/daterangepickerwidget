<?php

namespace Drupal\daterangepickerwidget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\daterangepickerwidget\DateRangePickerHelper;

/**
 * Plugin implementation of the 'daterangepicker_default' widget.
 *
 * @FieldWidget(
 *   id = "daterangepicker_default",
 *   label = @Translation("jQuery UI Date Range Picker Default"),
 *   module = "daterangepickerwidget",
 *   description = @Translation("Provides a jQuery UI widget similar to the date range picker used in Google Analytics"),
 *   field_types = {
 *     "daterangepicker"
 *   }
 * )
 */
class DateRangePickerDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#attributes' => [
        'class' => ['daterangepicker'],
        'autocomplete' => 'off',
      ],
      '#attached' => [
        'library' => [
          'daterangepickerwidget/jquery-ui-daterangepicker',
        ],
        'drupalSettings' => [
          'daterangepicker' => [],
        ],
      ],
    ];

    $config = $this->getDateRangePickerConfiguration();
    // Submitted date format - used inside JSON
    // { "start": "...", "end": "..." }.
    // This needs to be a format recognised by JavaScript Date.parse method.
    // NOTE: DO NOT change this value since we will use this format for the
    // internal storage format.
    $config['alt_format'] = 'yy-mm-dd';

    DateRangePickerHelper::setJavascriptApiOptions($element['#attached']['drupalSettings']['daterangepicker'], $config);

    return ['value' => $element];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + DateRangePickerHelper::getDefaultOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $default_values = $this->getDateRangePickerConfiguration();
    DateRangePickerHelper::buildOptionsForm($form, $default_values);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $config = $this->getDateRangePickerConfiguration();
    return array_merge($summary, DateRangePickerHelper::getSummary($config));
  }

  /**
   * Get the Date Range Picker configuration array.
   */
  protected function getDateRangePickerConfiguration() {
    $config = [];
    $options = array_keys(DateRangePickerHelper::getDefaultOptions());
    foreach ($options as $option) {
      $config[$option] = $this->getSetting($option);
      if (is_numeric($config[$option])) {
        settype($config[$option], 'integer');
      }
    }
    return $config;
  }

}
