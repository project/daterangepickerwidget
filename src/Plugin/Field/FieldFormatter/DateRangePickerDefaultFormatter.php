<?php

namespace Drupal\daterangepickerwidget\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation for the 'daterangepicker' fields.
 *
 * @FieldFormatter(
 *   id = "daterangepicker_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "daterangepicker"
 *   }
 * )
 */
class DateRangePickerDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $value = Json::decode($item->value);
      $datetime_start = new DrupalDateTime($value['start']);
      $datetime_end = new DrupalDateTime($value['end']);

      $elements[$delta] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['daterange'],
        ],
        'child' => [
                [
                  '#type' => 'html_tag',
                  '#tag' => 'time',
                  '#value' => $datetime_start->format($this->getSetting('date_format')),
                  '#attributes' => [
                    'class' => ['daterange_start'],
                    'datetime' => $datetime_start->format('Y-m-d'),
                  ],
                ],
                [
                  '#markup' => $this->getSetting('range_splitter'),
                ],
                [
                  '#type' => 'html_tag',
                  '#tag' => 'time',
                  '#value' => $datetime_end->format($this->getSetting('date_format')),
                  '#attributes' => [
                    'class' => ['daterange_end'],
                    'datetime' => $datetime_end->format('Y-m-d'),
                  ],
                ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_format' => 'j M, Y',
      'range_splitter' => ' - ',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['date_format'] = [
      '#title' => $this->t('Date format'),
      '#type' => 'textfield',
      '#description' => $this->t('See <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">the documentation for PHP date formats</a>.'),
      '#default_value' => $this->getSetting('date_format'),
    ];

    $form['range_splitter'] = [
      '#title' => $this->t('Range splitter'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('range_splitter'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Date format: @text', ['@text' => $this->getSetting('date_format')]);
    $date = new DrupalDateTime();
    $summary[] = $this->t('example: @text', ['@text' => $date->format($this->getSetting('date_format'))]);

    $summary[] = $this->t('Range splitter: @text', ['@text' => $this->getSetting('range_splitter')]);

    return $summary;
  }

}
