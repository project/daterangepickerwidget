<?php

namespace Drupal\daterangepickerwidget\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'daterangepicker' entity field type.
 *
 * @FieldType(
 *   id = "daterangepicker",
 *   label = @Translation("jQuery UI Date Range Picker"),
 *   module = "daterangepickerwidget",
 *   description = @Translation("An entity field containing a date range value."),
 *   default_widget = "daterangepicker_default",
 *   default_formatter = "daterangepicker_default"
 * )
 */
class DateRangePickerItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return is_null($value) || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Date range value'));

    return $properties;
  }

}
