<?php

namespace Drupal\daterangepickerwidget\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\daterangepickerwidget\DateRangePickerHelper;

/**
 * Provides a jQuery UI widget similar to the date range picker.
 *
 * @FormElement("daterangepicker")
 */
class DateRangePickerElement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $options = [];
    foreach (DateRangePickerHelper::getDefaultOptions() as $option => $value) {
      $options['#' . $option] = $value;
    }

    $class = static::class;
    return [
      '#input' => TRUE,
      '#empty_value' => '',
      '#default_value' => '',
      '#process' => [
        [$class, 'processDateRangePicker'],
      ],
      '#element_validate' => [
        [$class, 'validateDateRangePicker'],
      ],
    ] + $options;
  }

  /**
   * Process the Date Range Picker element.
   */
  public static function processDateRangePicker(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['daterangepicker'] = [
      '#type' => 'textfield',
      '#title' => $element['#title'] ?? '',
      '#title_display' => $element['#title_display'] ?? '',
      '#description' => $element['#description'] ?? '',
      '#default_value' => $element['#default_value'],
      // Set required to FALSE so that the widget validator
      // (::validateDateRangePicker) can take effect and we can show the error
      // message.
      '#required' => FALSE,
      '#attributes' => $element['#attributes'],
      '#attached' => [
        'library' => [
          'daterangepickerwidget/jquery-ui-daterangepicker',
        ],
        'drupalSettings' => [
          'daterangepicker' => [],
        ],
      ],
    ];

    // Manipulate classes on the text field (input element)
    $classes = $element['daterangepicker']['#attributes']['class'] ?? [];
    $element['daterangepicker']['#attributes']['class'] = array_merge(['daterangepicker'], $classes);

    // Manipulate classes for the text field label.
    if (isset($element['#required']) && $element['#required']) {
      $classes = $element['daterangepicker']['#label_attributes']['class'] ?? [];
      $element['daterangepicker']['#label_attributes']['class'] = array_merge($classes, [
        'js-form-required',
        'form-required',
      ]);
    }

    $config = static::getDateRangePickerConfiguration($element);
    // This value should not be altered by the user since we will use this
    // format for the internal storage format.
    $config['alt_format'] = 'yy-mm-dd';

    DateRangePickerHelper::setJavascriptApiOptions($element['daterangepicker']['#attached']['drupalSettings']['daterangepicker'], $config);
    return $element;
  }

  /**
   * Validate the Date Range Picker element.
   */
  public static function validateDateRangePicker(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = $form_state->getValue($element['#name']);
    if ($element['#required'] && empty($value)) {
      $form_state->setError($element, t('@field_title is required', ['@field_title' => $element['#title'] ?? $element['#name']]));
      $form_state->setValueForElement($element, '');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $ret = $element['#default_value'] ?? '';
    if ($input !== FALSE) {
      $input = $form_state->getUserInput('daterangepicker');
      $ret = Json::decode($input['daterangepicker']);
    }

    return $ret;
  }

  /**
   * Get the Date Range Picker configuration array.
   */
  protected static function getDateRangePickerConfiguration(array $element) {
    $config = [];
    $options = array_keys(DateRangePickerHelper::getDefaultOptions());
    foreach ($options as $option) {
      $config[$option] = $element['#' . $option];
      if (is_numeric($config[$option])) {
        settype($config[$option], 'integer');
      }
    }
    return $config;
  }

}
