**Integrating jQuery UI DateRangePicker with Better Exposed Filters**

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation

INTRODUCTION
------------
The jQuery UI DateRangePicker module provides integration with the 
"jQuery UI DateRangePicker" widget as a module. 
It offers a new field type, form element, and seamless integration with the 
Better Exposed Filters module for date fields. 
This integration allows for advanced date range selection options in 
Drupal Views.

For a full description of the module, visit the project page:
https://drupal.org/project/jquery_ui_daterangepicker

Requirements:
* jQuery UI DateRangePicker

INSTALLATION
------------
This module uses Asset Packagist to manage its JavaScript dependencies. 
To install the required JavaScript dependencies, set up your project's 
composer.json to integrate with Asset Packagist. 
For more information, refer to the following link:
https://github.com/drupal-composer/drupal-project/issues/278

To install the module and its dependencies, use Composer:
```
composer require drupal/jquery_ui_daterangepicker
```
